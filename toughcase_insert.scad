// Rectangular prism shaped insert for the DeWalt ToughCase system.
module Insert(
        insert_height, insert_width, insert_depth,
        // Tooth depth can be less than depth of the insert.  The bottom of the
        // insert will always be coplanar with the bottoms of the teeth.
        tooth_depth,
        // Translational offset -- as a fraction of the centroid-to-centroid
        // distance between adjacent teeth -- for the first tooth along the
        // axis of the toothed edge.  You may wish to manually tune this number
        // so that you don't end up with a small piece of tooth at the end that
        // breaks off easily.
        tooth_axial_offset_fraction = 0.1,
) {
    // Small jitter to prevent exact plane incidence, z-fighting, etc.
    eps_jitter = 1e-3;

    /*[ Tooth planar shape ]*/

    // Length of the short base of the smallest cross-section (small end of the
    // taper) of each tooth
    tooth_length_small_minor = 4.5;
    // Length of the long base of the smallest cross-section (small end of the
    // taper) of each tooth
    tooth_length_small_major = 6.0;
    // Height of each tooth
    tooth_height = 2;
    // Extra padding around the nominal perimeter of the tooth cutout, so that can
    // fit inside its mated tooth.
    tooth_padding = 0.1;

    /*[ Tooth tapering along depth dimension ]*/

    // Rate of the length-dimension taper on each tooth.  Meaning: for each 1 unit
    // in the depth direction, the lengths of the bases of the cross-section (an
    // isosceles trapezoid) both increase by this fraction of their original
    // length.  May be negative, in which case lengths decrease as depth value
    // increases.
    tooth_length_taper_slope = 0.02;

    /*[ Tooth arrangement ]*/

    // Distance between the centroids of adjacent teeth
    tooth_spacing = 11.5;

    tooth_axial_offset = tooth_axial_offset_fraction * tooth_spacing;

    intersection() {
        union() {
            translate([tooth_height, 0, 0])
            cube([insert_width - 2 * tooth_height,
                  insert_height,
                  insert_depth]);

            translate([tooth_height + eps_jitter,
                       tooth_axial_offset,
                       tooth_depth / 2])
            rotate([0, 0, 90])
            LineOfTeeth(min_length = insert_height,
                        depth = tooth_depth);

            translate([insert_width - tooth_height - eps_jitter,
                       tooth_axial_offset,
                       tooth_depth / 2])
            mirror([1, 0, 0])
            rotate([0, 0, 90])
            LineOfTeeth(min_length = insert_height,
                        depth = tooth_depth + 2 * eps_jitter);
        }
        cube([insert_width, insert_height, insert_depth]);
    }

    // Line of teeth starting at the origin and extending in the +x direction.  The
    // tooth's `tooth_height` dimension extends in the +y direction.  The depth
    // extends halfway in each of the +z and -z directions.  The length is slightly
    // more than `min_length`, can be clipped to an exact length by intersecting
    // with another object.
    //
    // The cross-section at the lowest depth value is a trapezoid with base lengths
    // `tooth_length_small_minor` and `tooth_length_small_major`.  As the depth
    // value increases, the base lengths of the cross-sections increase linearly
    // (with different slopes, proportional to the starting base length) so that
    // their ending values are `1 + tooth_length_taper_slope * depth` times their
    // starting values.
    module LineOfTeeth(min_length, depth) {
        let(offset_y = (tooth_height - tooth_padding) / 2,
                offset_z = 0)
            for(offset_x = [tooth_length_small_major / 2 - tooth_padding
                            : tooth_spacing
                            : min_length + tooth_spacing])
                translate([offset_x, offset_y, offset_z])
                linear_extrude(height = depth, center = true,
                               scale = [1 + tooth_length_taper_slope * depth,
                                        1])
                IsoscelesTrapezoid(
                    base1 = tooth_length_small_minor - 2 * tooth_padding,
                    base2 = tooth_length_small_major - 2 * tooth_padding,
                    height = tooth_height - tooth_padding);
    }
}

// ===== Helper modules =====

// Isosceles trapezoid with bases parallel to the x-axis and centroid at the
// origin
module IsoscelesTrapezoid(base1, base2, height) {
    polygon(points = [[-base2 / 2, height / 2],
                      [base2 / 2, height / 2],
                      [base1 / 2, -height / 2],
                      [-base1 / 2, -height / 2]]);
}


// ===== Example usage ====

Insert(insert_height = 60,
       insert_width = 20,
       insert_depth = 20,
       tooth_depth = 17);
