/* Inserts for the DeWalt ToughCase+ to hold Forstner bits.
 *
 * Case: DeWalt Medium ToughCase+ (p/n: DWAN2190)
 * Bits: WEN 8-piece Forstner bit set with crappy carrying case (p/n: FB3508)
 *
 * The largest bit (1-3/8" diameter) is too big to fit into the case even on
 * its own, so these inserts hold the other 7 bits.  The case has a median
 * (removable, but no need to remove it), so this model comprises two inserts:
 * one holding the four smaller bits (1/4", 3/8", 1/2", 5/8"), and the other
 * holding the three larger bits (3/4", 7/8", 1").  The insert holding the
 * larger bits has more depth and off-center pocket holes to provide enough
 * clearance for the heads of those bits to fit inside the case.
 *
 * When removing the inserts, best to use a right-angled pry tool (not hand)
 * positioned as close to the teeth as possible, so that you protect the teeth
 * by minimizing the torque on them.
 *
 * Fit history
 * ===========
 * - 2024-02-06, commit hash b1776a30:
 *       Fit is pretty good.  A little snug at the bottoms of the teeth, and
 *       juts up slightly near the median (perhaps could have made each insert
 *       another half millimeter narrower, i.e. width 74mm), but good enough.
 * - 2024-02-02, commit hash 548dd518:
 *       Teeth fit ok but a little loose (the sides probably ought to be more
 *       slanted).  The inserts are perhaps half a millimeter too wide, enough
 *       so that they create a bulge in the sides of the case.  Gotta at least
 *       make them narrower.
 */

use <toughcase_insert.scad>
use <util.scad>

module ForstnerBitHolderInsert(
    nominal_bit_diams_inches,
    insert_width = 74.5,
    insert_depth = 19,
    tooth_depth = 17,
    pocket_diameter = 10,
    pocket_height = 20,
    pocket_padding_bottom = 5,
    tooth_axial_offset_fraction = -0.05,
) {
    jitter = 1e-3;
    units_per_inch = 25.4;

    insert_height = pocket_height + pocket_padding_bottom;
    assert(pocket_diameter < insert_depth);

    bit_diameters = vclip(units_per_inch * nominal_bit_diams_inches,
                          pocket_diameter, 1e10);
    bit_diameters_cumul = cumsum(bit_diameters);
    bit_diameters_total = last(bit_diameters_cumul);

    // Bit spacing: how much space along the x-axis to put between the
    // x-extremes of neighboring bits.  We use that same spacing for the
    // distance between the leftmost/rightmost bits and the edge of the
    // insert.  And we choose the spacing so that the pockets span the
    // full width of the insert.
    bit_spacing = ((insert_width - bit_diameters_total)
                   / (len(bit_diameters) + 1));
    echo("bit_spacing = ", bit_spacing);
    assert(bit_spacing >= 0);
    bit_spacing_edge = bit_spacing;
    bit_spacing_between = bit_spacing;

    difference() {
        Insert(insert_height = insert_height,
               insert_width = insert_width,
               insert_depth = insert_depth,
               tooth_depth = tooth_depth,
               tooth_axial_offset_fraction = tooth_axial_offset_fraction);

        for(i = [0 : len(bit_diameters) - 1]) {
            let(pocket_center_x = bit_spacing_edge
                                  + bit_diameters_cumul[i]
                                  - bit_diameters[i] / 2
                                  + i * bit_spacing_between)
            translate([pocket_center_x,
                       pocket_padding_bottom + jitter,
                       max(insert_depth / 2, bit_diameters[i] / 2)])
            rotate([-90, 0, 0])
            cylinder(h = pocket_height,
                     d1 = pocket_diameter, d2 = pocket_diameter,
                     center = false,
                     $fn = 50);
        }
    }
}

ForstnerBitHolderInsert(nominal_bit_diams_inches = [1/4, 3/8, 1/2, 5/8]);

translate([0, 50, 0])
ForstnerBitHolderInsert(nominal_bit_diams_inches = [3/4, 7/8, 1],
                        insert_depth = 21);
