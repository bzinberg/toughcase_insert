/* Fit test for ToughCase insert (in particular, the interlocking teeth).
 *
 * History
 * =======
 * - 2024-02-06, commit hash b1776a30:
 *       Verified tooth shape by fit test on Medium ToughCase+ (p/n: DWAN2190).
 * - Long ago:
 *       Verified tooth spacing by fit test on the case for DeWalt drill bit
 *       set (p/n: DW1177).
 */

use <toughcase_insert.scad>
use <util.scad>

Insert(insert_height = 30,
       insert_width = 15,
       insert_depth = 20,
       tooth_depth = 17,
       tooth_axial_offset_fraction = -0.05);
