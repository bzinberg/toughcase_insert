function last(v) = v[len(v) - 1];

// Cumulative sum of an array, like `numpy.cumsum`.
function cumsum(v) = let(
    helper = function(v, i, accum_vec, accum_sum) (
        i == len(v) ?
        accum_vec :
        helper(v, i + 1,
               concat(accum_vec, [accum_sum + v[i]]),
               accum_sum + v[i])))
    helper(v, 0, [], 0);

// Like `numpy.clip(v, lo, hi)` where `v` is a vector and `lo` and `hi` are scalars.
// Example:
//     vclip([5, 12, 100], 10, 20)
// Result:
//     [10, 12, 20]
function vclip(v, lo, hi) = let(
    helper = function(v, lo, hi, i, accum_vec) (
        i == len(v) ?
        accum_vec :
        helper(v, lo, hi,
               i + 1,
               concat(accum_vec, max(lo, min(hi, v[i]))))))
    helper(v, lo, hi, 0, []);
